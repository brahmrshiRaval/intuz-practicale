# Intuz Practicale

Using the React framework, build a functioning calculator with a nice UI using only HTML, CSS and JavaScript. It does not need to handle all types of errors, but there should be some basic error reporting.

Requirements

1. Should be able to perform an operation on two numbers.
2. Basic error reporting.
3. Code is modular and bug-free.

How to submit
Please upload the code for this project to GitHub, and post a link to your repository below.
